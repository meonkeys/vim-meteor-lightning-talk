# Vim + Meteor talk

Slides for a talk on Vim and Meteor. To be presented on Worldwide Meteor Day at
the Seattle Meteor Meetup.

[View slideshow](http://wwmd-lightning-vim.meteor.com).

# Copyright and license

©2014 Adam Monsen

<!-- cut & pasted license HTML from creativecommons.org. Easy, and includes
"rel" attributes and metadata. -->

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a>

<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Vim Your Meteor For Awesome</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="http://adammonsen.com" property="cc:attributionName" rel="cc:attributionURL">Adam Monsen</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br />Permissions beyond the scope of this license may be available at <a xmlns:cc="http://creativecommons.org/ns#" href="mailto:haircut@gmail.com" rel="cc:morePermissions">mailto:haircut@gmail.com</a>.
