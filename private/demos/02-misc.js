var _api = {
'sum': sum,
'log': console.log
};

// add up numbers, return total
function sum(nums) {
var total = 0;
for (var i = 0, len = nums.length; i < len; i++) {
total += nums[i];
}
return total;
}

console.log(sum([5, 8, 3]));


// DEMO indent code with >iB
// DEMO indent code with = (better than >iB)
// see :help >
// see :help iB
// see :help =



// DEMO split windows horizontally, vertically
// see :help split



// DEMO edit file under cursor
// see :help gf

var file = "client/slides.html";



// DEMO UltiSnips, surround
// see https://github.com/SirVer/ultisnips
// see https://github.com/tpope/vim-surround

//var lorem = " lorem

//for loop

//new HTML file: html5, ul, li, span to div



// DEMO speed dating
// see https://github.com/tpope/vim-speeddating

var myDate = new Date('2013-12-31');
//                         ^


// DEMO
// see http://sjl.bitbucket.org/gundo.vim/


// mention YouCompleteMe
// http://www.alexeyshmalko.com/2014/youcompleteme-ultimate-autocomplete-plugin-for-vim/

// mention GitGutter
