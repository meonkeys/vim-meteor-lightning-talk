var defaultDate = new Date('2014-10-31')

if (Meteor.isClient) {
  Meteor.startup( function () {
    console.log("starting up client!", defaultDate);
    console.log("user:", Meteor.userId());
  }
}

// DEMO Syntastic
// see https://github.com/scrooloose/syntastic
// (write file, see warnings and errors)


// DEMO Tern smart-complete this:
//defaultDate.toLocaleTimeString


// DEMO add server code here
// (similar to client code, above)




// vim: number
